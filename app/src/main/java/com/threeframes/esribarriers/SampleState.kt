package com.threeframes.esribarriers

enum class SampleState {
    NotReady,
    Ready,
    AddingStops,
    AddingBarriers,
    Routing
}